function tentukanDeretGeometri(arr) {
    // you can only write your code here!
    var selisih = [];
    var benar = 0;
    if (arr[0]<arr[1]) {
        for(var i=0; i<(arr.length-1); i++){
            selisih +=arr[i+1]/arr[i];
        if (selisih.length > 1 && selisih [i] === selisih [i-1]){
            benar +=1;
            }
        }
        return  benar === (selisih.length-1);
    }
  }
  
  // TEST CASES
  console.log(tentukanDeretGeometri([1, 3, 9, 27, 81])); // true
  console.log(tentukanDeretGeometri([2, 4, 8, 16, 32])); // true
  console.log(tentukanDeretGeometri([2, 4, 6, 8])); // false
  console.log(tentukanDeretGeometri([2, 6, 18, 54])); // true
  console.log(tentukanDeretGeometri([1, 2, 3, 4, 7, 9])); // false
  